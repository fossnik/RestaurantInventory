const router = require('express').Router({});
const model = require("./models/recipe");

router.get('/', (req, res) => res.json({
	"Valid /api Routes": {
		"/recipes":     ["GET", "POST"],
		"/recipe/:_id": ["GET", "PUT", "DELETE"]
	}
}));

router.route('/recipes')
	.get(   (req, res) => model.getRecipes(req, res))
	.post(  (req, res) => model.addRecipe(req, res));

router.route('/recipe/:_id')
	.get(   (req, res) => model.getRecipe(req, res))
	.put(   (req, res) => model.putRecipe(req, res))
	.delete((req, res) => model.delRecipe(req, res));

module.exports = router;