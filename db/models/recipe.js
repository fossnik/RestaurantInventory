const mongoose = require('mongoose');
require("../database").connect(mongoose);

const Recipe = mongoose.model('Recipe', new mongoose.Schema({
  name: { type: String, required: true },
  ingredients: [{ item: String, qty: String }]
}));

module.exports.addRecipe = (req, res) =>
	new Recipe({
		name:        req.body.name,
		ingredients: req.body.ingredients
	}).save()
		.then(newItem => res.status(200).json({newItem}))
		.catch(err    => res.status(500).send(err));

module.exports.getRecipes = (req, res) =>
	Recipe.find({})
		.then(recipes => res.status(200).json(recipes))
		.catch(err    => res.status(500).send(err));

module.exports.getRecipe = (req, res) =>
	Recipe.findById(req.params._id)
		.then(recipe  => res.status(200).json(recipe))
		.catch(err    => res.status(500).send(err));

module.exports.delRecipe = (req, res) =>
	Recipe.remove({_id: req.params._id})
		.then(reply   => reply.ok ?
			res.status(200).send("DELETED")
			: Promise.reject())
		.catch(err    => res.status(500).send(err));

module.exports.putRecipe = (req, res) =>
	Recipe.findOneAndUpdate({_id: req.params._id}, {
		$set: {
			name:        req.body.name,
			ingredients: req.body.ingredients
		}})
		.catch(err    => res.status(500).json({"Not Updated": err}));
