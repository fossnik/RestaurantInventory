const getURI = () => 'mongodb://' +
	`${process.env.MONGOUSER || 'client'}:` +
	`${process.env.MONGOPASS || 'client'}@` +
	`${process.env.MONGOHOST || 'localhost'}:` +
	`${process.env.MONGOPORT || '27017'}/` +
	`${process.env.MONGONAME || 'richmond'}`;

module.exports.connect = (mongoose) => {
	mongoose.connect(getURI(), {useNewUrlParser: true})
		.catch(err => console.error(' - Database Connection Failed -\n', err));

	mongoose.connection.once('open', (conn = mongoose.connection) => {
		console.debug(`Connected ~${conn.host}:${conn.port}/${conn.name}~`);

		// errors won't log to console without this
		conn.on('error', console.error.bind(console, 'MongoDB error:\n'));

		// handle mongoose driver events
		conn.on('connected',    () => console.info('MongoDB - connected'));
		conn.on('disconnected', () => console.warn('MongoDB - disconnected'));
		conn.on('reconnected',  () => console.info('MongoDB - reconnected'));
	});
};