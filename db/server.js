const express = require('express');         // REST API
const bodyParser = require('body-parser');  // Object Parser
const morgan = require('morgan');           // API Request Logger
const cors = require('cors');               // Cross-Origin Read Support

// create an instance of express
const app = express();

// command express to use the 'bodyParser' middleware for JSON parsing
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

// command express to use 'morgan' to log requests in console
app.use(morgan('dev'));

// canonical route for all API calls
app.use('/api', cors(), require("./router"));

// catch invalid api routes
app.use((req, res) => res.status(404).send("Invalid Route"));

// start express server
app.listen(process.env.PORT || 3000);