module.exports = {
	entry: ['./src/index.js'],
	output: {
		path: __dirname,
		publicPath: '/',
		filename: 'bundle.js'
	},
	mode: 'development',
	module: {
		rules: [{
			exclude: /node_modules/,
			loader: 'babel-loader',
			query: {
				presets: ['@babel/preset-env', '@babel/preset-react']
			}
		}]
	},
	resolve: {
		extensions: ['.js', '.jsx']
	},
	devServer: {
		hot: false,
		inline: false,
		historyApiFallback: true,
		contentBase: './',
		watchOptions: {
			aggregateTimeout: 300,
			poll: 1000
		}
	}
};
