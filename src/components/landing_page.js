import React, { Component } from 'react'
import _ from 'lodash'
import { API_URL } from "../config"
import Table from "./common/table"

export default class LandingPage extends Component {
	constructor() {
		super();

		this.state = {
			recipes: null
		};
	}

	componentDidMount() {
		fetch(`${API_URL}/recipes`)
			.then(r => r.json().then(j => r.ok ? j : Promise.reject(j)))
			.then(response => this.setState({recipes: response}))
			.catch(err     => console.error('API Failure:', err))
	}

	render() {
		return !this.state.recipes ? <div>Loading...</div> :
			<div className="ui container">
				<h1 className="ui header medium">Build Your Own Inventory System</h1>
				<button className="ui button"
				        onClick={() => this.props.changePage({edit:null})}>New Recipe</button>
				<div className="ui cards">
					{this.products()}
				</div>
			</div>
	}

	products = () =>
		_.map(this.state.recipes, ({_id, name, ingredients}) =>
			<div className="card" key={_id}
				 onClick={() => this.props.changePage({_id})}>
				<div className="content">
					<div className="header">{name}</div>
					<div className="meta">recipe</div>
					<Table items={ingredients}/>
				</div>
			</div>
		);
}
