import React from 'react'

const Table = ({items}) =>
	items.length === 0 || !items[0].qty ? <div>Empty</div> :
	<table className="ui celled table">
		<tbody>
		<tr>
			<th>Item</th>
			<th>Quantity</th>
		</tr>
		{_.map(items, ({item, qty}) =>
			<tr key={item}>
				<td>{item}</td>
				<td>{qty}</td>
			</tr>
		)}
		</tbody>
	</table>;

export default Table