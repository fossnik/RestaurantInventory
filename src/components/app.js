import React, { Component } from 'react'
import LandingPage from "./landing_page"
import ViewRecipe from "./view_recipe"
import InputRecipe from "./input_recipe"

export default class App extends Component {
	constructor() {
		super();

		this.state = {
			page: null,
			_id: null
		};

		this.changePage = this.changePage.bind(this);
	}

	componentDidMount() {
		this.setState({page: 'landing'});
	}

	changePage = (param) => {
		if (param.page === 'landing')
			this.setState({page: 'landing'});

		else if (param._id)
			this.setState({
				page: 'viewRecipe',
				_id: param._id
			});

		else if (param.edit !== undefined)
			this.setState({
				page: 'editRecipe',
				_id: param.edit
			});

		else
			throw new Error('Invalid State')
	};

	render() {
		switch (this.state.page) {
			case 'landing':
				return <LandingPage changePage={this.changePage}/>;

			case 'viewRecipe':
				return <ViewRecipe _id={this.state._id} changePage={this.changePage}/>;

			case 'editRecipe':
				return <InputRecipe _id={this.state._id} changePage={this.changePage}/>;

			default:
				return <div>Invalid State</div>
		}
	}
}
