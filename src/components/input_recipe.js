import React, { Component } from 'react'
import { API_URL } from "../config"

export default class InputRecipe extends Component {
	constructor() {
		super();

		this.state = {
			name: '',
			ingredients: []
		};
	}

	componentDidMount() {
		if (this.props._id)
			fetch(`${API_URL}/recipe/${this.props._id}`)
				.then(r => r.json().then(j => r.ok ? j : Promise.reject(j)))
				.then(response => this.setState(
					{name: response.name, ingredients: response.ingredients}))
				.catch(err     => console.error('API Failure:', err));
		else
			this.setState({ingredients: [{item:'', qty:''}]});
	}

	render() {
		return this.state.ingredients.length === 0 ? <div>Loading...</div> :
			<div className="ui form">
				<div className="fields">
					<div className="field">
						<label>Recipe Name</label>
						<input defaultValue={this.state.name} type="text" id="name"/>
					</div>
					{_.map(this.state.ingredients, ({item, qty}) =>
						<div key={item}>
							<div className="field">
								<label>Item</label>
								<input defaultValue={item} type="text" className="itm"/>
							</div>
							<div className="field">
								<label>Quantity</label>
								<input defaultValue={qty} type="text" className="qty"/>
							</div>
						</div>
					)}
				</div>
				<div>
					<i className="large plus square icon"
					   onClick={() => this.setState(prevState => ({
						   ingredients: [...prevState.ingredients, {item:'', qty:''}]
					   }))}>New Ingredient</i>
				</div>
				<div className="ui button"
				     onClick={() => this.saveRecipe()}>Save Recipe</div>
			</div>
	}

	saveRecipe = () =>
		fetch(this.fetchURL(), this.fetchParams())
			.then(reply => reply.ok ? reply : Promise.reject(reply))
			.then(()    => this.props.changePage({page:'landing'}))
			.catch(err  => console.error('API Failure:', err));

	fetchURL = () => this.props._id ?
		  `${API_URL}/recipe/${this.props._id}`
		: `${API_URL}/recipes`;

	fetchParams = () => ({
		method: this.props._id ? 'PUT' : 'POST',
		headers: {'Content-Type': 'application/json'},
		body: JSON.stringify({
			"name": document.getElementById("name").value,
			"ingredients":
				_.zipWith( // remits an array of interpolated objects
				_.map(document.querySelectorAll('.itm'), ({value}) => value),
				_.map(document.querySelectorAll('.qty'), ({value}) => value),
				(item, qty) => ({item: item, qty: qty}))
		})
	});
}