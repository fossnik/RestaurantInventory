import React, { Component } from 'react'
import { API_URL } from "../config"
import Table from "./common/table"

export default class ViewRecipe extends Component {
	constructor() {
		super();

		this.state = {
			name: '',
			ingredients: undefined
		};
	}

	componentDidMount() {
		fetch(`${API_URL}/recipe/${this.props._id}`)
			.then(r => r.json().then(j => r.ok ? j : Promise.reject(j)))
			.then(response => this.setState(
				{name: response.name, ingredients: response.ingredients}))
			.catch(err     => console.error('API Failure:', err));
	}

	render() {
		return !this.state.ingredients ? <div>Loading...</div> :
			<div className="ui container">
				<h1>{this.state.name}</h1>
				<Table items={this.state.ingredients}/>
				{this.buttons()}
			</div>
	}

	buttons = () => <div className="ui container">
		<button className="negative ui button"
		        onClick={() => this.delRecipe()}>Delete Recipe</button>
		<button className="ui button"
		        onClick={() => this.props.changePage({edit:this.props._id})}>Edit Recipe</button>
	</div>;

	delRecipe = () =>
		fetch(`${API_URL}/recipe/${this.props._id}`, {method:'DELETE'})
			.then(reply => reply.ok ? reply : Promise.reject(reply))
			.then(()    => this.props.changePage({page:'landing'}))
			.catch(err  => console.error('API Failure:', err));
}